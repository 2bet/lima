const moment = require('moment')
const {
  knex
} = require('./db')

const entry = async (description, transactions) => {
  console.log(description, transactions)
  try {
    //validate amount
    
    const total = transactions.reduce( (total, trn) => {
        const debit = trn.debit ? trn.debit : 0 
        const credit = trn.credit ? trn.credit : 0
        return total + credit - debit
      }, 0)
    console.log('TOTAL:', total)
    if (total === 0) {
      console.log('saving the entry')
      console.log(transactions)
      await knex('private.posting').insert(transactions)
    } else {
      throw ('Debit and credit must sum to 0')
    }
  } catch (e) {
    console.log(e)
  }
}

const balance = async (accountCode) => {
  console.log(accountCode)
  const credit = await knex('private.posting')
    .sum('credit')
    .whereIn('account_id', knex('private.account')
      .select('id')
      .where({
        code: accountCode
      })
    )
    .first()
  const debit = await knex('private.posting')
    .sum('debit')
    .whereIn('account_id', knex('private.account')
      .select('id')
      .where({
        code: accountCode
      })
    )
    .first()
  console.log(credit.sum - debit.sum)
  return credit.sum - debit.sum
}
//balance('254722756923')

const ledger = async (accountCode, startDate = moment().subtract(365, 'days'), endDate = new Date()) => {
  console.log(accountCode, endDate)
  try {
    const posting = await knex('private.posting')
      .select('*')
      .whereBetween('date_added', [startDate, endDate])
      .whereIn('account_id', knex('private.account').select('id').where({
        code: accountCode
      }))
    console.log(posting)
    return posting
  } catch (e) {
    console.error(e)
  }
}
//console.log('Leger: ', ledger('254722756923'))

const voidEntry = async (account, description) => {
  console.log(account, description)
  return
}

module.exports = {
  entry,
  balance,
  ledger,
  voidEntry
}
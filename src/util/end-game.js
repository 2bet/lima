const moment = require('moment')
const { knex } = require('./db')
const { balance, entry } = require('./accounting')

const { sendWhatapp } = require('../msg/twilio')
const sendSMS = require('../msg/at')

const endGame = async () => {
  console.log('END GAME: ')
  const ended = await knex('private.placement')
    .select()
    .leftJoin('fixture', 'placement.fixture_id', 'fixture.fixture_id')
    .where({
      status: 'ENDED',
      processed: false,
      accepted: false
    })

  ended.map(async game => {
    console.log(game)
    const { id, market_code, score, party_id, bet_party_id, stake } = game


    const party = await knex('private.party').select().where({
      id: party_id
    }).first()
    const bettingParty = await knex('private.party').select().where({
      id: bet_party_id
    }).first()


    const account = await knex('private.account').select().where({
      code: party.phone_number
    }).first()


    const bettingAccount = await knex('private.account').select().where({
      code: bettingParty.phone_number
    }).first()

    if(
      (market_code === 'x' && score.home === score.away) ||
      (market_code === '1' && score.home > score.away) ||
      (market_code === '2' && score.home < score.away) 
    ){
      console.log(`${party.name} WON!`)
      //Process WIN
      settleAccount(true, account, bettingAccount, stake)

      await knex('private.placement').update({
        won: true
      }).where({
        id
      })

      broadcastWin(party, bettingParty, stake)
      broadcastLoss(party, bettingParty)
    } else {
      console.log(`${bettingParty.name} WON!`)
      settleAccount(false, account, bettingAccount, stake)

      broadcastWin(bettingParty, party, stake)
      broadcastLoss(bettingParty, party)
    }
    await knex('private.placement').update({
      processed: true,
      winning: stake * 1.6
    }).where({
      id
    })
  })

}

const settleAccount = (partyWon, account, bettingAccount, stake) => {
  entry('Bet', [
    {
      account_id: partyWon ? account.id : bettingAccount.id,
      journal_id: 'f6dd095e-eb59-49a1-adac-d8682e8280bc',
      credit: 0.8 * stake * 2
    },
    {
      account_id: '062920e3-49d6-42bb-a8da-45b590d83a87',
      journal_id: '575ddf37-2dc0-4b3f-a93d-bb7e9af6157d',
      debit: stake * 2
    },
    {
      account_id: '062920e3-49d6-42bb-a8da-45b590d83a87',
      journal_id: '575ddf37-2dc0-4b3f-a93d-bb7e9af6157d',
      credit: 0.2 * stake * 2
    },
  ])
}

const broadcastWin = async (winner, looser, stake) => {
  const bal = await balance(winner.phone_number)
  const msg = `Congratulation! ${winner.name}, You have you have won KES ${stake*1.6} bet against ${looser.name}. Your new balance is KES ${bal}`
  sendWhatapp(`+${winner.phone_number}`,msg)
  sendSMS(`+${winner.phone_number}`, msg)
}

const broadcastLoss = (winner, looser) => {
  const msg = `Opps! ${looser.name}, You have you lost a bet against ${winner.name}`
  sendWhatapp(`+${looser.phone_number}`, msg)
  sendSMS(`+${looser.phone_number}`, msg)
}

module.exports = endGame

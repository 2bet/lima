require('dotenv').config()
const { Pool } = require('pg')

const settings = {
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  port: process.env.PGPORT,
  user: process.env.PGUSER,
  port: process.env.PGPORT,
  password: process.env.PGPASSWORD
}
const pool = new Pool(settings)

module.exports = {
  pool,
  knex: require('knex')({
    client: 'pg',
    connection: settings
  })
}

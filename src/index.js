require('dotenv').config()
const cronJob = require('node-cron')
const { pool, knex } = require('./util/db')
const { balance, entry } = require('./util/accounting')
const { sendWhatapp } = require('./msg/twilio')
const sendSMS = require('./msg/at')

const endGame = require('./util/end-game')

// sendSMS('+254722756923', 'All systems up and running!')

pool.connect((err, client) => {
  if (err) throw err
  pgClient = client
  pgClient.on('notification', async msg => {
    const cwd = process.cwd()
    const payload = JSON.parse(msg.payload)    
    switch(msg.channel) {
      case 'watch_placement': {   
        console.log(cwd, payload)
        /* {
          "id":"5732dd9e-58dd-4e7c-b3b7-5586735ed0de",
          "party_id":"ef799cfe-ec05-4750-b0ed-133ca438c20c",
          "date_added":"2019-07-23T13:00:15.712695",
          "won":false,
          "fixture_id":65,
          "bet_party_id":"ef799cfe-ec05-4750-b0ed-133ca438c20c",
          "processed":false,
          "stake":50,
          "market_code":"1"
        } */
        // send notifications
        const bettingParty = await knex('private.party').select().where({
          id: payload.party_id
        }).first()

        // make a posting
        const currentBalance = await balance(bettingParty.phone_number)
        const account = await knex('private.account').select().where({
          code: bettingParty.phone_number
        }).first()

        console.log('BALANCE: ', currentBalance)
        console.log('ACCOUNT: ', account)

        if(currentBalance > payload.stake) {
          entry('Bet', [
            {
              account_id: account.id,
              journal_id: 'f6dd095e-eb59-49a1-adac-d8682e8280bc',
              debit: payload.stake
            },
            {
              account_id: '062920e3-49d6-42bb-a8da-45b590d83a87',
              journal_id: '575ddf37-2dc0-4b3f-a93d-bb7e9af6157d',
              credit: payload.stake
            },
          ])
        }

        
        sendWhatapp(`+${bettingParty.phone_number}`, `Hey ${bettingParty.name}, You have successfully placed a KES ${payload.stake} bet`)
        console.log(bettingParty)

        if(payload.bet_party_id) {
          const bettingPartner = await knex('private.party').select().where({
            id: payload.bet_party_id
          }).first()
          const msg = `Hey ${bettingPartner.name}, ${bettingParty.name} bets you with KES ${payload.stake} on a game. Click the link below to find out more. https://2bet.co.ke/bet`
          sendWhatapp(`+${bettingPartner.phone_number}`, msg)
          sendSMS(`+${bettingPartner.phone_number}`, msg)
          console.log(bettingPartner)
        }
      }
      default: {
          console .error(msg)
      }
    }
  })
  pgClient.query('LISTEN watch_placement')
});

const perSecondJobs = cronJob.schedule('*/15 * * * * *', function() {
  endGame()
}, null, true, 'Africa/Nairobi');


perSecondJobs.start()

const accountSid = 'AC77a0b452f989477beabc147fd3519871'; 
const authToken = '22ca8e5e771d15f205d9e6f4eae3e855'; 
const client = require('twilio')(accountSid, authToken); 

const sendWhatapp = (no, msg) => {
  client.messages 
  .create({ 
    body: msg, 
    from: 'whatsapp:+14155238886',       
    to: `whatsapp:${no}`
  }) 
  .then(message => console.log(message.sid)) 
  .done();
}

module.exports = {
  sendWhatapp
}
